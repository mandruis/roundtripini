# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2023-09-17
- Added formatting options for key/value pairs. Allows for customization of whitespace and more.
- Using `[]` can now return a section if only one key is provided.
- Documentation added for a lot of functions.
- INI can be created with just a string.

## [0.3.1] - 2023-05-12

### Fixed
- Fixed newline blowup on windows from fighting with python's universal newlines feature.

## [0.3.0] - 2022-09-06

### Added
- INI is now iterable (yields section names), and has a keys method for iterating over keys in a section.

### Fixed
- Files which do not end in a line separator can now be added to without mangling the file by omitting it
  after the last original entry.

## [0.2.0] - 2022-04-04 

### Changed
- The file argument to the `INI` class is now optional, to allow the creation of new ini files.
